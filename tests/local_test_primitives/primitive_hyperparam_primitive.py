import os.path

import numpy as np
import pandas as pd

from d3m import container, utils
from d3m.metadata import hyperparams, base as metadata_base
from d3m.primitive_interfaces import base, transformer
from tests.data.primitives.test_primitives.increment import IncrementPrimitive, Hyperparams as IncrementPrimitiveHyperparams

from . import __author__, __version__

__all__ = ('PrimitiveHyperparamPrimitive',)


# It is useful to define these names, so that you can reuse it both
# for class type arguments and method signatures.
Inputs = container.DataFrame
Outputs = container.DataFrame


class Hyperparams(hyperparams.Hyperparams):

    primitive = hyperparams.Hyperparameter[base.PrimitiveBase](
        default=IncrementPrimitive(hyperparams=IncrementPrimitiveHyperparams),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description='The primitive instance to be passed to PrimitiveHyperparamPrimitive'
    )

class PrimitiveHyperparamPrimitive(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    # It is important to provide a docstring because this docstring is used as a description of
    # a primitive. Some callers might analyze it to determine the nature and purpose of a primitive.

    """
    A primitive that requires a data argument hyperparam.
    """

    # This should contain only metadata which cannot be automatically determined from the code.
    metadata = metadata_base.PrimitiveMetadata({
        # Simply an UUID generated once and fixed forever. Generated using "uuid.uuid4()".
        'id': 'bd67f49a-bf10-4251-9774-019add57370b',
        'version': __version__,
        'name': "Primitive Hyperparam Test Primitive",
        # Keywords do not have a controlled vocabulary. Authors can put here whatever they find suitable.
        'keywords': ['test primitive'],
        'source': {
            'name': __author__,
            'contact': 'mailto:author@example.com',
            'uris': [
                # Unstructured URIs. Link to file and link to repo in this case.
                'https://gitlab.com/datadrivendiscovery/tests-data/blob/master/primitives/test_primitives/primitive_hyperparam_primitive.py',
                'https://gitlab.com/datadrivendiscovery/tests-data.git',
            ],
        },
        # A list of dependencies in order. These can be Python packages, system packages, or Docker images.
        # Of course Python packages can also have their own dependencies, but sometimes it is necessary to
        # install a Python package first to be even able to run setup.py of another package. Or you have
        # a dependency which is not on PyPi.
        'installation': [{
            'type': metadata_base.PrimitiveInstallationType.PIP,
            'package_uri': 'git+https://gitlab.com/datadrivendiscovery/tests-data.git@{git_commit}#egg=test_primitives&subdirectory=primitives'.format(
                git_commit=utils.current_git_commit(os.path.dirname(__file__)),
            ),
        }],
        # URIs at which one can obtain code for the primitive, if available.
        'location_uris': [
            'https://gitlab.com/datadrivendiscovery/tests-data/raw/{git_commit}/primitives/test_primitives/primitive_hyperparam_primitive.py'.format(
                git_commit=utils.current_git_commit(os.path.dirname(__file__)),
            ),
        ],
        # The same path the primitive is registered with entry points in setup.py.
        'python_path': 'd3m.primitives.operator.sum.PrimitiveHyperparamTest',
        # Choose these from a controlled vocabulary in the schema. If anything is missing which would
        # best describe the primitive, make a merge request.
        'algorithm_types': [
            metadata_base.PrimitiveAlgorithmType.COMPUTER_ALGEBRA,
        ],
        'primitive_family': metadata_base.PrimitiveFamily.OPERATOR,
        # A metafeature about preconditions required for this primitive to operate well.
    })

    def __init__(self, *, hyperparams: Hyperparams) -> None:
        super().__init__(hyperparams=hyperparams)

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
        primitive = self.hyperparams['primitive']
        result = primitive.produce(inputs=inputs)
        data = result.value
        if isinstance(data, pd.DataFrame):
            value = data.iloc[0]
        else:
            value = data[0]
        outputs = inputs + value
        return base.CallResult(outputs)
